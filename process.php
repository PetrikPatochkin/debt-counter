<?
// Author - Igor Tsihovskiy
// Date   - 26.03.2014
header('Content-type: text/html; charset: utf-8');

function output($array,$size) {
	echo "<table border=1 >";
	for($i=0;$i<$size;$i++){
		echo "<tr height=30>";
		for($j=0;$j<$size;$j++) {
			echo "<td width=75>".$array[$i][$j]."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";	
}

$names = $_POST['names'];
$paids = $_POST['paids'];

$round = (int)$_POST['round'];
$forgive = (int)$_POST['max-forgive'];

if((!$names)||(!$paids)) die("Абсолютно нет данных");
// Небольшая фильтрация
foreach($names as $k=>&$name){
	if(empty($name)) {
		unset($names[$k]);
		unset($paids[$k]);
	}
}
foreach($paids as &$paid){
	$paid = (float)$paid;
}
if((empty($names))||(empty($paids))) die("Информации на обработку нет. Введите имена");
// Построение матрицы общих долгов
$debt = array();
$k = count($paids);

for( $i=0; $i<$k; $i++ ) {
	for( $j=0; $j<$k; $j++ ) {
		$debt[$i][$j] = round(($paids[$j] - $paids[$i]) / $k , 2);
	}
}


// Упрощение схемы долгов
$simple = false;
while(!$simple) {
	$simple = false;
	
	for($i = 0;$i<$k;$i++){
		
		for($j=0;$j<$k;$j++){
			if($i==$j) continue;
			// Поиск пар
			for($z=$j+1;$z<$k;$z++){
				if($z == 0) continue;
				// Проверка на возможность упрощения
				if(($debt[$i][$z]*$debt[$i][$j]<0)&&($debt[$i][$z]+$debt[$i][$j]>=0)){
					// Проверка на наличие связи между связанными с рассматриваемым
					if($debt[$j][$z] != 0){
						//echo $i." ".$j." ".$z." !!! ".$debt[$i][$j]." ".$debt[$i][$z]."<br>";
						if($debt[$i][$j]<0){
							// Уменьшение долга рассматриваемого
							$debt[$i][$z] += $debt[$i][$j];
							// Обновление долга между связанными
							$debt[$z][$j] += $debt[$i][$j];
							// Погашение долга рассматриваемому
							$debt[$i][$j] = 0;
							// Симметричные связи
							$debt[$z][$i] = -$debt[$i][$z];
							$debt[$j][$z] = -$debt[$z][$j];
							$debt[$j][$i] = $debt[$i][$j];
						} else {
							// Уменьшение долга рассматриваемого
							$debt[$i][$j] += $debt[$i][$z];
							// Обновление долга между связанными
							$debt[$j][$z] += $debt[$i][$z];
							// Погашение долга рассматриваемому
							$debt[$i][$z] = 0;
							// Симметричные связи
							$debt[$j][$i] = -$debt[$i][$j];
							$debt[$z][$j] = -$debt[$j][$z];
							$debt[$z][$i] = $debt[$i][$z];
						}
						break 3;
					}
				}
			}
		}
		$simple = true;
	}
}
// Вывод долгов
//output($debt,$k);
// Вывод только долгов с именами
$flag = false;
	echo "<b>Долги:</b><br>";
	echo "<table>";
	for($i=0;$i<$k;$i++){
		for($j=0;$j<$k;$j++) {
		echo "<tr>";
			if($debt[$i][$j]>$forgive) {
				echo "<td width=200>".$names[$i]." должен ".$names[$j]."</td><td>".round($debt[$i][$j],$round)."</td>";
				$flag=true;
			}
		echo "</tr>";
		}
	}
	echo "</table>";
if(!$flag) echo "Долгов нет";
?>